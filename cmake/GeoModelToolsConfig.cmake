get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/GeoModelTools-ExpressionEvaluator.cmake)
include(${SELF_DIR}/GeoModelTools-JSONParser.cmake)
include(${SELF_DIR}/GeoModelTools-XMLParser.cmake)

